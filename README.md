# PFPA-Backend

Main project website: https://pfpa.gitlab.io

This is the backend and frontend which is useless with at least one preference allocator. This backend is
responsible for providing data to the frontend and the allocator.

We will use [Django](https://www.djangoproject.com/) and an openapi-specification to provide the data for both
componenents. The openapi-spec is generated through the [djangorestframework](https://www.django-rest-framework.org/).
