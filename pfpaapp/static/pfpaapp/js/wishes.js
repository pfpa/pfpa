function field_changer(hide) {
    if (hide) {
        $('#div_id_thirdchoice').hide()
        $('#div_id_fourthchoice').hide()
        $('#div_id_firstexchangechoice').hide()
        $('#div_id_secondexchangechoice').hide()
        $('#div_id_exclusionchoice').hide()
    } else {
        $('#div_id_thirdchoice').show()
        $('#div_id_fourthchoice').show()
        $('#div_id_firstexchangechoice').show()
        $('#div_id_secondexchangechoice').show()
        $('#div_id_exclusionchoice').show()
    }
}

function additum_change() {
    if($("#id_additum").prop("checked") === true) {
            field_changer(true)
        } else {
            field_changer(false)
        }
}


$(document).ready(function () {
    additum_change()
    $("#id_additum").change(function () {
        additum_change()
    });
});