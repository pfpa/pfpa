$(document).ready(function () {
    change_enabled_status(true)

    $('#id_wishes_confirm_form').submit(function () {
        change_enabled_status(false)
        console.log("Change status...")
        return true;
    });
});

function change_enabled_status(status) {
    $('#id_additum').prop('disabled', status)
    $('#id_firstchoice').prop('disabled', status)
    $('#id_secondchoice').prop('disabled', status)
    $('#id_thirdchoice').prop('disabled', status)
    $('#id_fourthchoice').prop('disabled', status)
    $('#id_firstexchangechoice').prop('disabled', status)
    $('#id_secondexchangechoice').prop('disabled', status)
    $('#id_exclusionchoice').prop('disabled', status)
}
