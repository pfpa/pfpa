"""
This is the module to test our django application automatically.
"""
from django.test import TestCase

from pfpaapp.models import Sex


class SexModelTests(TestCase):
    def test_name_correct(self):
        s = Sex(name="Male")
        s.save()
        self.assertIsNotNone(s.id)
