"""
This is the Django representation of all database models which we have in our application. Adjustments here need to be
reflected via Django Migrations.
"""
import calendar
from datetime import date

from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
from django_prometheus.models import ExportModelOperationsMixin

# This is WIP and thus it is not useful for now.

WEEKDAYS = [
    (0, 'Monday'),
    (1, 'Tuesday'),
    (2, 'Wednesday'),
    (3, 'Thursday'),
    (4, 'Friday'),
    (5, 'Saturday'),
    (6, 'Sunday')
]

SEX_NAME_VALIDATOR = RegexValidator(
    regex=r"^[a-zA-Z]*$",
    message="Please enter a name with 1 - 25 letters.",
    code='invalid_name')


class Sex(ExportModelOperationsMixin('sex'), models.Model):
    """
    This represents a gender of an object. Since in sport classes this is relevant for grades we need to have this
    information also for calculation available.
    """
    name = models.CharField(max_length=25,
                            help_text='This is the name of the gender which someone has.',
                            validators=[SEX_NAME_VALIDATOR])

    def __str__(self):
        return self.name


class ClassSlot(ExportModelOperationsMixin('classslot'), models.Model):
    """
    Stores a single class slot entry which is available for teachers to pick and pupils to choose from if the course is
    available multiple times.
    """
    begin = models.TimeField()
    end = models.TimeField()
    dayOfWeek = models.IntegerField(choices=WEEKDAYS)

    def __str__(self):
        return "%s %s - %s" % \
               (calendar.day_abbr[self.dayOfWeek],
                self.begin.isoformat(timespec='minutes'),
                self.end.isoformat(timespec='minutes'))


class ClassOfferGroup(ExportModelOperationsMixin('classoffergroup'), models.Model):
    """
    Stores the group which a Class offer belongs to.
    """
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class ClassOffer(ExportModelOperationsMixin('classoffer'), models.Model):
    """
    Stores a single class offer entry which is defining all information about a class.
    """
    name = models.CharField(max_length=50)
    group = models.ForeignKey(ClassOfferGroup, on_delete=models.DO_NOTHING)
    maxCapacity = models.IntegerField()
    minCapacity = models.IntegerField()
    description = models.TextField(null=True, blank=True)
    slot = models.ForeignKey(ClassSlot, on_delete=models.DO_NOTHING)
    year = models.IntegerField()
    intendedSex = models.ForeignKey(Sex, on_delete=models.DO_NOTHING, null=True, blank=True)

    def __str__(self):
        return "%s: %s" % (self.name, str(self.slot))


class Teacher(ExportModelOperationsMixin('teacher'), models.Model):
    """
    This picks up a Django User and then ties all class offers the teacher has to it.
    """
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)
    classOffers = models.ManyToManyField(ClassOffer, related_name='teachers')

    def __str__(self):
        return self.user.username


class Pupil(ExportModelOperationsMixin('pupil'), models.Model):
    """
    This picks up a Django User and then ties the sex as well as all wishes to it.
    """
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)
    sex = models.ForeignKey(Sex, on_delete=models.DO_NOTHING)
    wish_submitted = models.DateField(default=date.fromtimestamp(0))
    wish_additum = models.BooleanField(default=False)

    def has_submitted_wishes(self):
        """
        Weather a pupil has already submitted his wishes or not. This works through checking if the date is older then
        the default entered in the database as a default.

        :return: True if this is the case, otherwise not.
        """
        return self.wish_submitted > date.fromtimestamp(0)

    def __str__(self):
        return self.user.username


class WishCategory(ExportModelOperationsMixin('wishcategory'), models.Model):
    """
    This decides how many and what type of whish a pupil can wish for. It also arranges the order on the wish pages.
    """
    name = models.CharField(max_length=50, unique=True)
    order = models.IntegerField(unique=True)
    category_type = models.CharField(max_length=5, choices=[
        ('C', 'Choice'),
        ('S', 'Swap Choice'),
        ('E', 'Exclusion Choice')
    ])

    def __str__(self):
        return self.name

    def simplified_name(self):
        """
        This generated a str which can be used as a key to a dict for example and still has meaning to an admin.

        :return: The name of the object. Spaces will get replaced by underscores and everything will be in lowercase.
        """
        return self.name.replace(" ", "_").lower()


class Wish(ExportModelOperationsMixin('wish'), models.Model):
    """
    This is a combination of a teacher and a class slot. A ClassOffer may be possibly held by multiple teachers where
    a pupil favors one teacher.
    """
    classOffer = models.ForeignKey(ClassOffer, on_delete=models.DO_NOTHING)
    teacher = models.ForeignKey(Teacher, null=True, blank=True, on_delete=models.DO_NOTHING)
    pupil = models.ForeignKey(Pupil, on_delete=models.DO_NOTHING)
    category = models.ForeignKey(WishCategory, on_delete=models.DO_NOTHING)

    def __str__(self):
        if self.teacher:
            return "%s - %s" % (str(self.classOffer), str(self.teacher))
        return "%s - None" % str(self.classOffer)


class ClassAllocation(ExportModelOperationsMixin('classallocation'), models.Model):
    """
    This is a possible allocation of a class after the optimizer has run.
    """
    teacher = models.OneToOneField(Teacher, on_delete=models.DO_NOTHING)
    slot = models.OneToOneField(ClassSlot, on_delete=models.DO_NOTHING)
    pupil = models.ForeignKey(Pupil, on_delete=models.DO_NOTHING)
    lesson = models.OneToOneField(ClassOffer, on_delete=models.DO_NOTHING)


class OptimizerRun(ExportModelOperationsMixin('optimizerrun'), models.Model):
    """
    This is a collection of all information which is handed over to the optimizer.
    """
    pupils = models.ManyToManyField(Pupil)
    classOffers = models.ForeignKey(ClassOffer, on_delete=models.DO_NOTHING)
    wishes = models.ForeignKey(Wish, on_delete=models.DO_NOTHING)


class OptimizerResult(ExportModelOperationsMixin('optimizerresult'), models.Model):
    """
    This is the result of the optimizer. It contains a possible version of how the classes are arranged in the most
    optimal way.
    """
    classAllocation = models.ForeignKey(ClassAllocation, on_delete=models.DO_NOTHING)
