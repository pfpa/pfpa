"""
This contains all serializers which are used in the REST-API from the ``rest_framework``.
"""

from django.contrib.auth.models import Group
from rest_framework import serializers

from pfpaapp.models import User, Sex, ClassSlot, ClassOffer, Teacher, Wish, Pupil, ClassAllocation, OptimizerRun
from pfpaapp.models import OptimizerResult, WishCategory


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize User objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Group objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = Group
        fields = ['url', 'name']


class SexSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Sex objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = Sex
        fields = ['url', 'name']


class ClassSlotSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Class Slot objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = ClassSlot
        fields = ['url', 'begin', 'end', 'dayOfWeek']


class ClassOfferGroupSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Class Offer Group objects.
    """

    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = ClassOffer
        fields = ['url', 'name', 'description']


class ClassOfferSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Class Offer objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = ClassOffer
        fields = ['url', 'name', 'maxCapacity', 'minCapacity', 'description', 'slot', 'year', 'intendedSex']


class TeacherSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Teacher objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = Teacher
        fields = ['url', 'user', 'classOffers']


class WishCategorySerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize WishCategory objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = WishCategory
        fields = ['name', 'order', 'category_type']


class WishSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Wish objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = Wish
        fields = ['url', 'classOffer', 'teacher']


class PupilSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Pupil objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = Pupil
        fields = ['url', 'user', 'sex']


class ClassAllocationSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Class Allocation objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = ClassAllocation
        fields = ['url', 'teacher', 'slot', 'pupil', 'lesson']


class OptimizerRunSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Optimizer Run objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = OptimizerRun
        fields = ['url', 'pupils', 'classOffers', 'wishes']


class OptimizerResultSerializer(serializers.HyperlinkedModelSerializer):
    """
    The serializer class for the ``rest_framework`` to enable it to serialize Optimizer Run objects.
    """
    class Meta:
        """
        This is a meta class to supplement the ``rest_framework`` with the needed information.
        """
        model = OptimizerResult
        fields = ['url', 'classAllocation']
