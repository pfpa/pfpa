from django.contrib.auth.models import User
from import_export import resources

from pfpaapp.models import Pupil, Teacher, ClassSlot, ClassOfferGroup, ClassOffer, Sex, WishCategory


class WishCategoryResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """

    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = WishCategory


class PupilResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = Pupil


class TeacherResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = Teacher


class ClassSlotResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = ClassSlot


class ClassOfferGroupResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = ClassOfferGroup


class ClassOfferResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = ClassOffer


class UserResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = User


class SexResource(resources.ModelResource):
    """
    This resource class has the purpose to enable the Import/Export package to do its job.
    """
    class Meta:
        """
        Meta class to fulfill the functionality of ``django-import-export``.
        """
        model = Sex
