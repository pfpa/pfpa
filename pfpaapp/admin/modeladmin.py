from django.contrib.auth.admin import UserAdmin
from import_export.admin import ImportExportModelAdmin

from pfpaapp.admin.resources import PupilResource, TeacherResource, ClassSlotResource, ClassOfferGroupResource
from pfpaapp.admin.resources import ClassOfferResource, UserResource, SexResource, WishCategoryResource


class WishCategoryAdmin(ImportExportModelAdmin):
    """
    The WishCategory Admin which adjusts Django Admin to our needs.
    """
    resource_class = WishCategoryResource
    list_display = ('order', 'name', 'category_type')
    ordering = ('order',)


class PupilAdmin(ImportExportModelAdmin):
    """
    The Pupil Admin which adjusts Django Admin to our needs.
    """
    resource_class = PupilResource
    list_display = ('username', 'sex')
    fields = ('user', 'sex')

    def username(self, obj):
        """
        This function retrieves the username from the user object for Django Admin.

        :param obj: The user object of the pupil.
        :return: The username of the person. May be an empty string.
        """
        return obj.user.username

    username.short_description = 'Username'


class TeacherAdmin(ImportExportModelAdmin):
    """
    The Teacher Admin which adjusts Django Admin to our needs.
    """
    resource_class = TeacherResource
    list_display = ('username', 'last_name')

    def username(self, obj):
        """
        This function retrieves the username from the user object for Django Admin.

        :param obj: The user object of the teacher.
        :return: The username of the person. May be an empty string.
        """
        return obj.user.username

    username.short_description = 'Username'

    def last_name(self, obj):
        """
        This function retrieves the last name from the user object for Django Admin.

        :param obj: The user object of the teacher.
        :return: The last_name of the person. May be an empty string.
        """
        return obj.user.last_name

    last_name.short_description = 'Last Name'


class ClassSlotAdmin(ImportExportModelAdmin):
    """
    The Class Slot Admin which adjusts Django Admin to our needs.
    """
    resource_class = ClassSlotResource
    list_display = ('dayOfWeek', 'begin', 'end')


class ClassOfferGroupAdmin(ImportExportModelAdmin):
    """
    The Class Offer Group Admin which adjusts Django Admin to our needs.
    """
    resource_class = ClassOfferGroupResource
    list_display = ('name',)


class ClassOfferAdmin(ImportExportModelAdmin):
    """
    The Class Offer Admin which adjusts Django Admin to our needs.
    """
    resource_class = ClassOfferResource
    list_display = ('name', 'description', 'minCapacity', 'maxCapacity', 'slot', 'year', 'group', 'intendedSex')


class CustomUserAdmin(ImportExportModelAdmin, UserAdmin):
    """
    The Custom User Admin which adjusts Django Admin to our needs.
    """
    resource_class = UserResource


class SexAdmin(ImportExportModelAdmin):
    """
    The Sex Admin which adjusts Django Admin to our needs.
    """
    resource_class = SexResource
