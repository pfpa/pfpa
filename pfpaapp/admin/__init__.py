"""
This module is thought for customizing the Django admin page reachable under `http(s)://<HOST>/admin`.
"""
from django.contrib import admin

from pfpaapp.admin.modeladmin import SexAdmin, TeacherAdmin, PupilAdmin, ClassOfferGroupAdmin, ClassOfferAdmin
from pfpaapp.admin.modeladmin import CustomUserAdmin, WishCategoryAdmin, ClassSlotAdmin
from pfpaapp.admin.resources import *

admin.site.register(Sex, SexAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Pupil, PupilAdmin)
admin.site.register(ClassOfferGroup, ClassOfferGroupAdmin)
admin.site.register(ClassOffer, ClassOfferAdmin)
admin.site.register(ClassSlot, ClassSlotAdmin)
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(WishCategory, WishCategoryAdmin)
