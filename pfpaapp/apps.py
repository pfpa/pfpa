"""
The main module for the application we are building.
"""

from django.apps import AppConfig


class PfpaappConfig(AppConfig):
    """
    This is the App Config for our main app in this project.
    """
    name = 'pfpaapp'
