"""
pfpaapp URL configuration
"""

from django.conf.urls import url
from django.urls import path, include
from django.contrib.auth import views as auth_views
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from . import views

schema_view = get_schema_view(
    openapi.Info(
        title="Internal and External PFPA API",
        default_version='v1',
        description="This API exposes all components which the GUI also exposes.",
        contact=openapi.Contact(email="matrixfueller@gmail.com"),
        license=openapi.License(name="GNU GPLv3"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

app_name = 'pfpaapp'

account_patterns = [
    path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='registration/logout.html'), name='logout'),
    path('overview', views.overview, name='overview'),
    path('password_change', auth_views.PasswordChangeView.as_view(template_name='registration/password_change.html'),
         name='password_change'),
    path('password_change/done', auth_views.PasswordChangeDoneView.as_view(),
         name='password_change_done'),
    path('password_reset', auth_views.PasswordResetView.as_view(template_name='registration/password_reset.html'),
         name='password_reset'),
    path('password_reset/done', auth_views.PasswordResetCompleteView.as_view(),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('reset/done', auth_views.PasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
]

wish_patterns = [
    path('', views.wishes_index),
    path('submit', views.wishes_submit, name='wishes_submit'),
    path('confirm', views.wishes_confirm, name='wishes_confirm'),
    path('edit', views.wishes_edit, name='wishes_edit'),
    path('show', views.wishes_show, name='wishes_show'),
]

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('accounts/', include(account_patterns)),
    path('wishes/', include(wish_patterns))
]
