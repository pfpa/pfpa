"""
This module contains all views which can be searches through in the UI.
"""
import logging
from datetime import date

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.contrib.auth.models import Group
from rest_framework import viewsets, permissions

from pfpaapp.models import User, Sex, ClassSlot, ClassOffer, Teacher, Wish, Pupil, ClassAllocation, OptimizerRun
from pfpaapp.models import OptimizerResult, ClassOfferGroup, WishCategory
from pfpaapp.forms import WishForm, WishConfirmForm
from pfpaapp.serializers import UserSerializer, GroupSerializer, SexSerializer, ClassSlotSerializer
from pfpaapp.serializers import ClassOfferSerializer, TeacherSerializer, WishSerializer, PupilSerializer
from pfpaapp.serializers import ClassAllocationSerializer, OptimizerRunSerializer, OptimizerResultSerializer
from pfpaapp.serializers import ClassOfferGroupSerializer, WishCategorySerializer

logger = logging.getLogger(__name__)


def __build_navbar_context(request):
    """
    This fills the context with the objects needed to render the navbar correctly.

    :param request: The request coming from the user/browser.
    :return: The dictionary which contains either a pupil or a teacher if a user is authenticated. Otherwise it is
             empty.
    """
    context = {}
    if request.user.is_authenticated:
        current_user = request.user
        if Pupil.objects.filter(user=current_user).exists():
            context['pupil'] = Pupil.objects.get(user=current_user)
        elif Teacher.objects.filter(user=current_user).exists():
            context['teacher'] = Teacher.objects.get(user=current_user)
        else:
            logger.info('Neither a pupil nor a teacher has logged in. Affected username: %s', current_user)
    return context


def __build_bound_confirm_form(data_ids: dict):
    form_values = {'additum': data_ids.pop('additum', False)}
    for key in data_ids:
        if data_ids[key]:
            offer_id = data_ids[key]
            form_values.update({key: ClassOffer.objects.get(id=offer_id).name})
        else:
            form_values.update({key: ''})
    return form_values


def __generate_wish_dict(wish_form_instance):
    return {
        'additum': wish_form_instance.cleaned_data.get("additum", False),
        'firstchoice': wish_form_instance.cleaned_data.get("firstchoice", ""),
        'secondchoice': wish_form_instance.cleaned_data.get("secondchoice", ""),
        'thirdchoice': wish_form_instance.cleaned_data.get("thirdchoice", ""),
        'fourthchoice': wish_form_instance.cleaned_data.get("fourthchoice", ""),
        'firstexchangechoice': wish_form_instance.cleaned_data.get("firstexchangechoice", ""),
        'secondexchangechoice': wish_form_instance.cleaned_data.get("secondexchangechoice", ""),
        'exclusionchoice': wish_form_instance.cleaned_data.get("exclusionchoice", "")
    }


def __generate_wish_form_session_data(request, clean_wishes: dict):
    request.session['wish_form'] = {
        'additum': clean_wishes['additum'],
        'firstchoice': clean_wishes['firstchoice'].id if clean_wishes['firstchoice'] else "",
        'secondchoice': clean_wishes['secondchoice'].id if clean_wishes['secondchoice'] else "",
        'thirdchoice': clean_wishes['thirdchoice'].id if clean_wishes['thirdchoice'] else "",
        'fourthchoice': clean_wishes['fourthchoice'].id if clean_wishes['fourthchoice'] else "",
        'firstexchangechoice': clean_wishes['firstexchangechoice'].id if clean_wishes['firstexchangechoice'] else "",
        'secondexchangechoice': clean_wishes['secondexchangechoice'].id if clean_wishes['secondexchangechoice'] else "",
        'exclusionchoice': clean_wishes['exclusionchoice'].id if clean_wishes['exclusionchoice'] else "",
    }


def __generate_wish(session, field_name: str, pupil):
    try:
        offer_id = session['wish_form'][field_name]
        if not offer_id:
            return None
        offer_object = ClassOffer.objects.get(id=offer_id)
        try:
            offer_object.full_clean()
            logger.info("\"%s\" of pupil \"%s\" is valid.", field_name, pupil.user.username)
        except ValidationError:
            logger.warning("\"%s\" of user \"%s\" is invalid.", field_name, pupil.user.username)
        return Wish(classOffer=offer_object, pupil=pupil)
    except ClassOffer.DoesNotExist:
        return None


def index(request):
    """
    The landing page for users.

    :template:`pfpaapp/index.html`
    """
    context = {**__build_navbar_context(request)}
    return render(request, 'pfpaapp/index.html', context)


@login_required
def wishes_index(request):
    """
    This redirects to :view:`wishes_submit`
    """
    return redirect('wishes_submit')


@login_required
def wishes_submit(request):
    """
    This is the page where a pupil can submit its wishes.

    :template:`pfpaapp/wishes/submit.html`
    """
    context = __build_navbar_context(request)

    if context['pupil'].has_submitted_wishes():
        return redirect('pfpaapp:wishes_edit')

    if request.method == 'POST':
        # if this is a POST request we need to process the form data
        form = WishForm(request.POST, **{'user': request.user})
        # create a form instance and populate it with data from the request:
        if form.is_valid():
            clean_wish_dict = __generate_wish_dict(form)
            if clean_wish_dict['additum'] is None:
                # This should never happen since the form data is cleaned and optional fields get a value. If it happens
                # we show the user an error.
                form.add_error(None, "Unbekannter Fehler bei der Verarbeitung des Additum Wertes.")
            elif isinstance(clean_wish_dict['additum'], bool) and clean_wish_dict['additum']:
                if clean_wish_dict['firstchoice'] and clean_wish_dict['secondchoice'] \
                        and not (clean_wish_dict['thirdchoice']
                                 or clean_wish_dict['fourthchoice']
                                 or clean_wish_dict['firstexchangechoice']
                                 or clean_wish_dict['secondexchangechoice']
                                 or clean_wish_dict['exclusionchoice']):
                    __generate_wish_form_session_data(request, clean_wish_dict)
                    return redirect('pfpaapp:wishes_confirm')
                else:
                    # TODO: Error because wrong choice
                    pass
            elif isinstance(clean_wish_dict['additum'], bool) and not clean_wish_dict['additum']:
                if clean_wish_dict['firstchoice'] and clean_wish_dict['secondchoice']:
                    __generate_wish_form_session_data(request, clean_wish_dict)
                    return redirect('pfpaapp:wishes_confirm')
                else:
                    # TODO: Some error maybe. Think this through
                    pass
            else:
                # This should also never happen. Give the user a general error message.
                form.add_error(None, "Unbekannter Fehler bei der Verarbeitung der Wahldaten.")
    else:
        # if a GET (or any other method) we'll create a blank form
        form = WishForm(**{'user': request.user})
    context = {**context, **{'form': form}}
    return render(request, 'pfpaapp/wishes/submit.html', context)


@login_required
def wishes_confirm(request):
    """
    This is the page where a pupil confirms what he has chosen as wishes.

    :template:`pfpaapp/wishes/confirm.html`
    """
    # Check if the required session variable is there.
    if "wish_form" not in request.session:
        logger.info("No session for the form data not in wishes confirm!")
        return redirect("pfpaapp:wishes_submit")

    if request.method == 'POST':
        if Pupil.objects.filter(user=request.user).exists():
            pupil = Pupil.objects.get(user=request.user)
        else:
            return redirect('pfpaapp:index')

        if pupil.has_submitted_wishes():
            return redirect('pfpaapp:wishes_edit')

        if request.POST.get("back", None):
            return redirect("pfpaapp:wishes_submit")

        form = WishConfirmForm(request.POST)
        if form.is_valid():
            # Save to the Database
            # Don't uncomment save() calls before we are safe this works!
            current_data = __generate_wish_dict(form)
            session_data = __build_bound_confirm_form(request.session["wish_form"])
            if session_data == current_data:
                pupil.wish_additum = session_data["additum"]
                pupil.wish_submitted = date.today()
                try:
                    pupil.full_clean()
                    logger.info("Pupil is valid.")
                    pupil.save()
                except ValidationError:
                    logger.warning("Pupil is not valid.")
                    # TODO: Redirect to error page
                firstchoice = __generate_wish(request.session, "firstchoice", pupil)
                if firstchoice is not None:
                    firstchoice.category = WishCategory.objects.get(order=1)
                    firstchoice.save()
                    pass
                else:
                    # TODO: Redirect to error page
                    pass
                secondchoice = __generate_wish(request.session, "secondchoice", pupil)
                if secondchoice is not None:
                    secondchoice.category = WishCategory.objects.get(order=2)
                    secondchoice.save()
                    pass
                else:
                    # TODO: Redirect to error page
                    pass
                if not session_data["additum"]:
                    thirdchoice = __generate_wish(request.session, "thirdchoice", pupil)
                    if thirdchoice is not None:
                        thirdchoice.category = WishCategory.objects.get(order=3)
                        thirdchoice.save()
                        pass
                    fourthchoice = __generate_wish(request.session, "fourthchoice", pupil)
                    if fourthchoice is not None:
                        fourthchoice.category = WishCategory.objects.get(order=4)
                        fourthchoice.save()
                        pass
                    firstexchangechoice = __generate_wish(request.session, "firstexchangechoice", pupil)
                    if firstexchangechoice is not None:
                        firstexchangechoice.category = WishCategory.objects.get(order=5)
                        firstexchangechoice.save()
                        pass
                    secondexchangechoice = __generate_wish(request.session, "secondexchangechoice", pupil)
                    if secondexchangechoice is not None:
                        secondexchangechoice.category = WishCategory.objects.get(order=6)
                        secondexchangechoice.save()
                        pass
                    exclusionchoice = __generate_wish(request.session, "exclusionchoice", pupil)
                    if exclusionchoice is not None:
                        exclusionchoice.category = WishCategory.objects.get(order=7)
                        exclusionchoice.save()
                        pass
                    logger.info(
                        "User \"%s\" has not choosen additum and choosen the following wishes: 1. \"%s\" and 2. \"%s\""
                        " and 3. \"%s\" and 4. \"%s\", exchanges are 1. \"%s\" and 2. \"%s\", exclusion is \"%s\".",
                        request.user.username, firstchoice, secondchoice, thirdchoice, fourthchoice,
                        firstexchangechoice, secondexchangechoice, exclusionchoice)
                else:
                    logger.info(
                        "User \"%s\" has choosen additum and choosen the following wishes: 1. \"%s\" and 2. \"%s\".",
                        request.user.username, firstchoice, secondchoice)
                del request.session["wish_form"]
                logger.info("Wishes were successfully submitted for User \"%s\".", request.user.username)
                return redirect("pfpaapp:index")
            else:
                del request.session["wish_form"]
                logger.warning("Data was tampered with between request. Throwing away session.")
                return redirect("pfpaapp:wishes_submit")
        else:
            logger.info("Submitted WishConfirmForm was not valid")
            logger.info("request.POST: %s", request.POST.keys())
            logger.warning("Form error: %s", form.errors)
            return redirect("pfpaapp:wishes_submit")
    elif request.method == 'GET':
        context = __build_navbar_context(request)

        if context['pupil'].has_submitted_wishes():
            redirect('pfpaapp:wishes_edit')

        form = WishConfirmForm(__build_bound_confirm_form(request.session["wish_form"]))
        context = {**context, **{'form': form}}
        return render(request, 'pfpaapp/wishes/confirm.html', context)
    else:
        return redirect("pfpaapp:wishes_submit")


@login_required
def wishes_edit(request):
    """
    This is the page where a pupil edits what he has already chosen his wishes.

    :template:`pfpaapp/wishes/edit.html`
    """
    context = __build_navbar_context(request)
    if request.method == 'POST':
        # if this is a POST request we need to process the form data
        form = WishForm(request.POST)
        # create a form instance and populate it with data from the request:
        if form.is_valid():
            # check whether it's valid:
            # process the data in form.cleaned_data as required
            # ...
            # TODO: Code
            # redirect to a new URL:
            return redirect('wishes_confirm')
    else:
        # if a GET (or any other method) we'll create a blank form
        form = WishForm()
    context = {**context, **{'form': form}}
    return render(request, 'pfpaapp/wishes/edit.html', context)


@login_required
def wishes_show(request):
    """
    This is the page where someone can show what his wishes are.

    :template:`pfpaapp/wishes/show.html`
    """
    context = __build_navbar_context(request)
    wishes = list(Wish.objects.filter(pupil=context["pupil"]))
    context = {**context, **{"wishes": wishes}}
    return render(request, 'pfpaapp/wishes/show.html', context)


def overview(request):
    """
    This is the page where someone can view the public statistics of our application.

    :template:`pfpaapp/overview.html`
    """
    context = {**__build_navbar_context(request)}
    return render(request, 'pfpaapp/overview.html', context)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class SexViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sexes to be viewed or edited.
    """
    queryset = Sex.objects.all()
    serializer_class = SexSerializer
    permission_classes = [permissions.IsAuthenticated]


class ClassSlotViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows class slots to be viewed or edited.
    """
    queryset = ClassSlot.objects.all()
    serializer_class = ClassSlotSerializer
    permission_classes = [permissions.IsAuthenticated]


class ClassOfferGroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows class offers groups to be viewed or edited.
    """
    queryset = ClassOfferGroup.objects.all()
    serializer_class = ClassOfferGroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class ClassOfferViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows class offers to be viewed or edited.
    """
    queryset = ClassOffer.objects.all()
    serializer_class = ClassOfferSerializer
    permission_classes = [permissions.IsAuthenticated]


class TeacherViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows teachers to be viewed or edited.
    """
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = [permissions.IsAuthenticated]


class WishCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wishes from pupils to be viewed or edited.
    """
    queryset = WishCategory.objects.all()
    serializer_class = WishCategorySerializer
    permission_classes = [permissions.IsAuthenticated]


class WishViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wishes from pupils to be viewed or edited.
    """
    queryset = Wish.objects.all()
    serializer_class = WishSerializer
    permission_classes = [permissions.IsAuthenticated]


class PupilViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows pupils to be viewed or edited.
    """
    queryset = Pupil.objects.all()
    serializer_class = PupilSerializer
    permission_classes = [permissions.IsAuthenticated]


class ClassAllocationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows class allocations to be viewed or edited.
    """
    queryset = ClassAllocation.objects.all()
    serializer_class = ClassAllocationSerializer
    permission_classes = [permissions.IsAuthenticated]


class OptimizerRunViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows optimizer runs to be viewed or edited.
    """
    queryset = OptimizerRun.objects.all()
    serializer_class = OptimizerRunSerializer
    permission_classes = [permissions.IsAuthenticated]


class OptimizerResultViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows optimizer results to be viewed or edited.
    """
    queryset = OptimizerResult.objects.all()
    serializer_class = OptimizerResultSerializer
    permission_classes = [permissions.IsAuthenticated]
