"""
This is the module for Django forms. Here we define them and customize them so we can use the Django built-in
helpers for serverside (and clientside) validation.
"""
import logging

from django import forms
from django.db.models import Q

from pfpaapp.models import ClassOffer, Pupil

logger = logging.getLogger(__name__)


def class_offer_all():
    """
    Get all class offers. This is just here to get the same set of objects for all choices.

    :return: All ClassOffer objects.
    """
    return ClassOffer.objects.all()


class WishForm(forms.Form):
    """
    This is the form with which pupils will choose their desired PE subjects.

    Related to :view:`pfpaapp.wishes_submit`
    """
    additum = forms.BooleanField(label='Additum', required=False)
    firstchoice = forms.ModelChoiceField(queryset=class_offer_all(), label='1. Einzelsportart')
    secondchoice = forms.ModelChoiceField(queryset=class_offer_all(), label='2. Mannschaftssportart')
    thirdchoice = forms.ModelChoiceField(queryset=class_offer_all(), label='3. Sportart', required=False)
    fourthchoice = forms.ModelChoiceField(queryset=class_offer_all(), label='4. Sportart', required=False)
    firstexchangechoice = forms.ModelChoiceField(queryset=class_offer_all(), label='1. Ersatzsportart', required=False)
    secondexchangechoice = forms.ModelChoiceField(queryset=class_offer_all(), label='2. Ersatzsportart', required=False)
    exclusionchoice = forms.ModelChoiceField(queryset=class_offer_all(), label='Ausschlusssportart', required=False)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(WishForm, self).__init__(*args, **kwargs)
        if user is not None:
            current_pupil = Pupil.objects.get(user=user)
            class_offer_current_sex = ClassOffer.objects.filter(Q(intendedSex=current_pupil.sex) | Q(intendedSex=None))
            group_a = class_offer_current_sex.filter(group=1)
            group_b = class_offer_current_sex.filter(group=2)
            self.fields['firstchoice'].queryset = group_a
            self.fields['secondchoice'].queryset = group_b
            self.fields['thirdchoice'].queryset = class_offer_current_sex
            self.fields['fourthchoice'].queryset = class_offer_current_sex
            self.fields['firstexchangechoice'].queryset = class_offer_current_sex
            self.fields['secondexchangechoice'].queryset = class_offer_current_sex
            self.fields['exclusionchoice'].queryset = class_offer_current_sex

    @property
    def media(self):
        return forms.Media(js=('/static/pfpaapp/js/wishes.js',))


class WishConfirmForm(forms.Form):
    """
    This is the form with which pupils will confirm their choosing.

    Related to :view:`pfpaapp.wishes_confirm`
    """
    additum = forms.BooleanField(label='Additum', required=False)
    firstchoice = forms.CharField(label='1. Einzelsportart')
    secondchoice = forms.CharField(label='2. Mannschaftssportart')
    thirdchoice = forms.CharField(label='3. Sportart', required=False)
    fourthchoice = forms.CharField(label='4. Sportart', required=False)
    firstexchangechoice = forms.CharField(label='1. Ersatzsportart', required=False)
    secondexchangechoice = forms.CharField(label='2. Ersatzsportart', required=False)
    exclusionchoice = forms.CharField(label='Ausschlusssportart', required=False)

    @property
    def media(self):
        return forms.Media(js=('/static/pfpaapp/js/wishes.js', '/static/pfpaapp/js/wishes_confirm.js'))

    def __clean_field(self, name):
        data = self.cleaned_data[name]
        try:
            ClassOffer.objects.get(name=data)
        except ClassOffer.DoesNotExist:
            raise forms.ValidationError("The ClassOffer \"%s\" does not exist!" % name)
        return data

    def clean_firstchoice(self):
        """
        This is calling the private method ``__clean_field(self, name)`` to clean the first choice.

        :return: The value if the field is clean.
        :raises ClassOffer.DoesNotExist: If field is not clean.
        """
        return self.__clean_field('firstchoice')

    def clean_secondchoice(self):
        """
        This is calling the private method ``__clean_field(self, name)`` to clean the first choice.

        :return: The value if the field is clean.
        :raises ClassOffer.DoesNotExist: If field is not clean.
        """
        return self.__clean_field('secondchoice')
