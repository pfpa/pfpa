"""pfpa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from pfpaapp import views

apiRouter = routers.DefaultRouter()
apiRouter.register(r'users', views.UserViewSet)
apiRouter.register(r'groups', views.GroupViewSet)
apiRouter.register(r'sex', views.SexViewSet)
apiRouter.register(r'classslot', views.ClassSlotViewSet)
apiRouter.register(r'classoffergroup', views.ClassOfferGroupViewSet)
apiRouter.register(r'classoffer', views.ClassOfferViewSet)
apiRouter.register(r'teacher', views.TeacherViewSet)
apiRouter.register(r'wish', views.WishViewSet)
apiRouter.register(r'pupil', views.PupilViewSet)
apiRouter.register(r'classallocation', views.ClassAllocationViewSet)
apiRouter.register(r'optimizerrun', views.OptimizerRunViewSet)
apiRouter.register(r'optimizerresult', views.OptimizerResultViewSet)
apiRouter.register(r'wishcategory', views.WishCategoryViewSet)

urlpatterns = [
    path('', include('pfpaapp.urls')),
    path('api/', include((apiRouter.urls, 'api'))),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    url('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url('', include(('django_prometheus.urls', 'prometheus'))),
]
