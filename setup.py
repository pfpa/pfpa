"""A setuptools based setup module.

See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pfpa-backend',
    version='0.0.1',
    description='Backend for the PFPA application which calculates school-classes according to provided teacher and '
                'pupil wishes.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/pfpa/backend',
    author='The PFPA Authors',
    # TODO: Create E-Mail-Address
    # author_email='pypa-dev@googlegroups.com',
    classifiers=[
        'Development Status :: 3 - Alpha',

        'Intended Audience :: System Administrators',
        'Topic :: Other/Nonlisted Topic',

        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',

        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='pfpa prefenceallocation school',
    package_dir={'pfpa-backend': '.'},
    packages=find_packages(where='pfpa-backend'),
    python_requires='>=3',
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[
        'setuptools',
        'Django',
        'djangorestframework',
        'pyyaml',
        'uritemplate',
        'docutils',
        'drf-yasg',
        'django-import-export',
        'django-prometheus',
        'django-crispy-forms'
    ],
    extras_require={
        'test': ['coverage', 'pytest'],
        'lint': ['flake8', 'pylint', 'pylint-django']
    },

    # If there are data files included in your packages that need to be
    # installed, specify them here.
    # package_data={  # Optional
    #    'sample': ['package_data.dat'],
    # },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],  # Optional

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    # entry_points={  # Optional
    #    'console_scripts': [
    #        'sample=sample:main',
    #    ],
    # },
    project_urls={
        'Bug Reports': 'https://gitlab.com/pfpa/backend/issues',
        'Source': 'https://gitlab.com/pfpa/backend',
    },
)
